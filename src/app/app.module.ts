import { TokenCheckserviceService } from './services/token-checkservice.service';
import { FileUploadServiceService } from './services/file-upload-service.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';


import { AppRoutingModule } from './app-routing/app-routing.module';
import { AdminModule } from './admin/admin.module';
import { AuthModule } from './auth/auth.module';

import { AppComponent } from './app.component';
import { AuthService } from './services/auth.service';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AuthModule,
    AdminModule,
    NgbModule.forRoot()
  ],
  providers: [
    AuthService,
    FileUploadServiceService,
    TokenCheckserviceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
