import { NgModule, TemplateRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { contratosRouting } from './contratos.routing';

//modulos del proyecto
import { CuotaAparteComponent } from './automaticos/proporcionales/cuota-aparte/cuota-aparte.component';

//servicios

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(contratosRouting),
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
      CuotaAparteComponent
  ],
  providers: []
})
export class ContratosModule { }
