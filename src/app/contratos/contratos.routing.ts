import { CuotaAparteComponent } from './automaticos/proporcionales/cuota-aparte/cuota-aparte.component';

export const contratosRouting = [
    {
        path: 'cuotaAparte',
        component: CuotaAparteComponent
    },
    {
        path: '**',
        redirectTo: 'home'
    }
];
