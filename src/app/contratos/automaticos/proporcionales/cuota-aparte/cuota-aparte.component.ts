import { Observable } from 'rxjs/Observable';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { } from 'jquery';
import { } from 'simple-money-format';
import { } from 'morris.js';
import { } from 'jquery-knob';
import { } from 'bootstrap-datepicker';
import { } from 'simple-money-format';
import { } from 'jqueryui';
import { } from 'daterangepicker';
import { } from 'jquery.slimscroll';
import * as moment from 'moment';
import {LocalStorageService, SessionStorageService} from 'ngx-webstorage';


//import { Rsltncntrts } from '../../../../../../Models/rsltncntrts';
import { HelperService } from '../../../../Cores/Services/helper.service';
import { NgForm, NgControl } from '@angular/forms';
@Component({
  selector: 'app-cuota-aparte',
  templateUrl: './cuota-aparte.component.html',
  styleUrls: ['./cuota-aparte.component.css'],
  providers: [HelperService]
})
export class CuotaAparteComponent implements OnInit {
  calendarfi: JQuery;
  calendarff: JQuery;
  number: JQuery;
  item = { c: '', e: '', r: '' };
  frmValues = { d: '', fi: '', ff: '', mn: '', s: '', o: '', sl1: '', cs1: '', re1: '', sl2: '', cs2: '', re2: '', sl3: '', cs3: '', re4: '' };
  d = '';
  currency: Observable<any>;
  rl: string = '/rsltncntrts';
  modulo: string = 'Cuota Parte';



  constructor(private router: Router, private _currency: HelperService, private service: HelperService) {
    this._currency.path = '/rsltnmnds';
    //this.currency = _currency.get().;
    if (localStorage.getItem('rsltntmpcntrt') === null) {
      this.item.e = '';
      this.item.r = ''
      this.item.c = '';

    } else {
      this.item = JSON.parse(localStorage.getItem('rsltntmpcntrt'));
      this.item.e = this.item.r;
    }

    if (sessionStorage.getItem('cntrt') === null) {
      this.frmValues = { d: '', fi: '', ff: '', mn: '', s: '', o: '', sl1: '', cs1: '', re1: '', sl2: '', cs2: '', re2: '', sl3: '', cs3: '', re4: '' };
    } else {
      this.frmValues = JSON.parse(sessionStorage.getItem('cntrt'));
      // console.log(this.frmValues);
      // this.d = this.frmValues.d;
    }

  }

  ngOnInit() {
    //this.number = jQuery('#siniestros').simpleMoneyFormat();

    this.calendarfi = jQuery('#fi').datepicker({
      format: 'yyyy-mm-dd'
    });
    this.calendarff = jQuery('#ff').datepicker({
      format: 'yyyy-mm-dd'
    });



  }


  ngOnDestroy() {

    // sessionStorage.removeItem('rsltntmpcntrt');
  }

  goDetail(fmr) {
    fmr.fi = this.calendarfi.val().toString();
    fmr.ff = this.calendarff.val().toString();
    sessionStorage.setItem('cntrt', JSON.stringify(fmr));
    console.log(sessionStorage.getItem('cntrt'));
    this.router.navigate(['admin/contratos/automaticos/proporcionales/cuota-aparte/detalle']);
  }


  create(item) {
    this._currency.path = this.rl;
    this.service.post(item)
      .subscribe(
        item => {
          //   console.log(item[0].mensaje);
          alert(item.item.mensaje);
          this.router.navigate(['/admin/contratos']);
        },
        error => console.log(<any>error));
  }

}