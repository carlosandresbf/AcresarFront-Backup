import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

//import { AuthService } from '../Servicios/auth-service';
import { Router } from '@angular/router';

@Injectable()
export class PublicGuard implements CanActivate {
  constructor(
    //public _authService: AuthService, 
    public _router: Router){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    
    /**if(!this._authService.isLogin()){
        return true;
    }*/
    console.log('usuario autenticado')

    this._router.navigate(['/home'])
    return false;
  }
}
