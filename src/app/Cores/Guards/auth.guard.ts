import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
//import { AuthService } from '../../service/auth.service'
import { Router } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {
 
 
  constructor(
    //public _authService: AuthService, 
    public _router: Router) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    /*if(this._authService.isLogin()){
      return true;
    }*/
    console.log('acceso denegado');
    this._router.navigate(['/login']);
    return false;
      //return true;
  }
}
