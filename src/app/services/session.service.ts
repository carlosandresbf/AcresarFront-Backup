import { Injectable } from '@angular/core';
import {LocalStorageService, SessionStorageService} from 'ngx-webstorage';

@Injectable()
export class SessionService {

  constructor(
    private storage:LocalStorageService
  ) { }

  guard(key:string, data:any){
    if ( this.storage.isStorageAvailable ){
      this.storage.store(key, data);
    }
  }
  getData(key:string){
    return this.storage.retrieve(key);
  }
  clearData(key:string){
    this.storage.clear(key); 
  }
}
