import { TestBed, inject } from '@angular/core/testing';

import { RsltnpssService } from './rsltnpss.service';

describe('RsltnpssService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RsltnpssService]
    });
  });

  it('should be created', inject([RsltnpssService], (service: RsltnpssService) => {
    expect(service).toBeTruthy();
  }));
});
