import { TestBed, inject } from '@angular/core/testing';

import { RsltnrsgrdrsService } from './rsltnrsgrdrs.service';

describe('RsltnrsgrdrsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RsltnrsgrdrsService]
    });
  });

  it('should be created', inject([RsltnrsgrdrsService], (service: RsltnrsgrdrsService) => {
    expect(service).toBeTruthy();
  }));
});
