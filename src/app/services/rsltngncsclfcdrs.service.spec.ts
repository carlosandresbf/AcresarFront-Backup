import { TestBed, inject } from '@angular/core/testing';

import { RsltngncsclfcdrsService } from './rsltngncsclfcdrs.service';

describe('RsltngncsclfcdrsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RsltngncsclfcdrsService]
    });
  });

  it('should be created', inject([RsltngncsclfcdrsService], (service: RsltngncsclfcdrsService) => {
    expect(service).toBeTruthy();
  }));
});
