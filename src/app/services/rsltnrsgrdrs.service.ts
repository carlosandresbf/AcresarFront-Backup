import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { TokenCheckserviceService } from './token-checkservice.service';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class RsltnrsgrdrsService {
  constructor(private http: HttpClient, public _auth: AuthService, public _checkToken: TokenCheckserviceService, private router: Router) {
    this._checkToken.check_token_session();
  }

 
}