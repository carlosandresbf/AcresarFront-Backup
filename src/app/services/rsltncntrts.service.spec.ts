import { TestBed, inject } from '@angular/core/testing';

import { RsltncntrtsService } from './rsltncntrts.service';

describe('RsltncntrtsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RsltncntrtsService]
    });
  });

  it('should be created', inject([RsltncntrtsService], (service: RsltncntrtsService) => {
    expect(service).toBeTruthy();
  }));
});
