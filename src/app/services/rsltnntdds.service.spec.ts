import { TestBed, inject } from '@angular/core/testing';

import { RsltnntddsService } from './rsltnntdds.service';

describe('RsltnntddsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RsltnntddsService]
    });
  });

  it('should be created', inject([RsltnntddsService], (service: RsltnntddsService) => {
    expect(service).toBeTruthy();
  }));
});
