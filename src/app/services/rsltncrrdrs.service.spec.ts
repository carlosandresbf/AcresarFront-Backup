import { TestBed, inject } from '@angular/core/testing';

import { RsltncrrdrsService } from './rsltncrrdrs.service';

describe('RsltncrrdrsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RsltncrrdrsService]
    });
  });

  it('should be created', inject([RsltncrrdrsService], (service: RsltncrrdrsService) => {
    expect(service).toBeTruthy();
  }));
});
