import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuPrimaComponentComponent } from './menu-prima-component.component';

describe('MenuPrimaComponentComponent', () => {
  let component: MenuPrimaComponentComponent;
  let fixture: ComponentFixture<MenuPrimaComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuPrimaComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuPrimaComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
