import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ajustes-reaseguros-list',
  templateUrl: './ajustes-reaseguros-list.component.html',
  styleUrls: ['./ajustes-reaseguros-list.component.css']
})
export class AjustesReasegurosListComponent implements OnInit {

  modulo: string = 'Ajustes Reaseguros';
  constructor() { }

  ngOnInit() {
  }

}
