import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjustesReasegurosListComponent } from './ajustes-reaseguros-list.component';

describe('AjustesReasegurosListComponent', () => {
  let component: AjustesReasegurosListComponent;
  let fixture: ComponentFixture<AjustesReasegurosListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjustesReasegurosListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjustesReasegurosListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
