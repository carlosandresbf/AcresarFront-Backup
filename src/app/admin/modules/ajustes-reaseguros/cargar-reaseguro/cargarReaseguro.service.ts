import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { HelperService } from '../../../../services/helper.service';

@Injectable()
export class CargarReaseguroService {

    constructor(
        public _http: HelperService
    ) { }

    get() {
        return this._http.getQuery('/entidades');
    }

    post(item: any){
        console.log(item);
        return this._http.postQuery(item, '');
    }

}
