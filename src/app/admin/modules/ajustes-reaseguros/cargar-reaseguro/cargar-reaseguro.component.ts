import { Component, OnInit } from "@angular/core";
import { HelperService } from "../../../../services/helper.service";
import { UploadEvent, UploadFile } from "ngx-file-drop";
import { CargarReaseguroService } from "./cargarReaseguro.service";
import { FileUploadServiceService } from "../../../../services/file-upload-service.service";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: "app-cargar-reaseguro",
  templateUrl: "./cargar-reaseguro.component.html",
  styleUrls: ["./cargar-reaseguro.component.css"],
  providers: [HelperService, CargarReaseguroService]
})
export class CargarReaseguroComponent implements OnInit {
  public inputFileModel: Array<any> = new Array<any>();
  public inputFileMinimalModel: Array<any> = new Array<any>();
  FileStore: any;
  f: FormGroup;
  fileToUpload: File = null;
  filename: string;
  ext: string;
  readyToSend: boolean = false;
  reacodexResaguradores: any;
  compania: any;
  cargars: any = [
    {
      a: 1,
      c: "Todo"
    },
    {
      a: 2,
      c: "Ramos"
    },
    {
      a: 3,
      c: "Productos"
    }
  ];

  constructor(
    private http: CargarReaseguroService,
    private _upload: FileUploadServiceService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.loadService();

    this.f = this.formBuilder.group({
      inputFile: [null, [Validators.required]],
      typeFileInput: [null, [Validators.required]]
    });
  }

  private handleFileInput(event) {
    this.fileToUpload = event.target.files[0];
    this.filename = this.f.controls["inputFile"].value.split(".");
    this.ext = this.filename[this.filename.length - 1].toLowerCase();
    if (this.ext == "xls" || this.ext == "xlsx") {
      this.f.controls["typeFileInput"].setValue(this.ext);
      this.readyToSend = true;
    } else {
      alert("Solo se permiten archivos de excel (xls, xlsx).");
      this.f.reset();
      this.readyToSend = false;
    }
  }

  loadService() {
    this.http.get().subscribe(res => {
      this.compania = res;
    });
    console.log(this.compania, this.cargars);
  }

  public onAccept(file: any): void {
    this.FileStore = file;
  }

  public onRemove(file: any): void {
    this.FileStore = "";
  }

  SendFile() {
    const fb: FormData = new FormData();
    console.log(this.FileStore.file, this.FileStore.file.name);
    fb.append("fileName", this.FileStore.file, this.FileStore.file.name);
    fb.append("modelu", "primaCarga");
    this._upload.uploadFile(fb).subscribe(
      data => {
        console.log("4");
      },
      error => {
        console.log(error);
      }
    );
  }
}
