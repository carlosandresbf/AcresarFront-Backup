import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CargarReaseguroComponent } from './cargar-reaseguro.component';

describe('CargarReaseguroComponent', () => {
  let component: CargarReaseguroComponent;
  let fixture: ComponentFixture<CargarReaseguroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CargarReaseguroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CargarReaseguroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
