import { Component, OnInit } from '@angular/core';
import { MenuPrimaComponentComponent } from '../menu-prima-component/menu-prima-component.component';
import { HelperService } from '../../../../services/helper.service';

@Component({
  selector: 'app-primas-component',
  templateUrl: './primas-component.component.html',
  styleUrls: ['./primas-component.component.css'],
  providers: [HelperService]
})

export class PrimasComponentComponent implements OnInit {

  modulo = 'Prima';
  money: any;
  mon: any;
  sucursales: any;
  compania: any;
  dataForm: any;
  poliza: string;
  moneda;
  ///

  certificado ;
  modeda ;
  inicio = '';
  fin ;
  sucursalesSelect;
  companiaSelect  ;
  ramo  ;
  descripcion ;
  sumaAsegurada ;
  sumaDistribucion ;
  prima;
  identificacion;
  fecha1;
  fecha2;

  tabla = {
    contrato: '',
    tramo: '',
    sumaRetencion: '',
    sumaCedida: '',
    primaRetenida: '',
    primaCedida: ''
  };
  ramotecnico;
  primaDistribucion;


  constructor(
    public http: HelperService
  ) { }

  ngOnInit() {
    this.cargarServicios();
  }

  cargarServicios() {
    //money
    this.http.getQuery('/rsltnmnds').subscribe(
      data => {
        this.money = data;
      },
      err => {
        console.log(err);
      }
    );
    //sucursales
    this.http.getQuery('/rsltnplzsscrsls').subscribe(
      res => {
        this.sucursales = res;
      },
      err => {
        console.log(err);
      }
    );
    //companias
    this.http.getQuery('/rsltnntdds').subscribe(
      res => {
        this.compania = res;
      },
      err => {
        console.log(err);
      }
    );
  }

  changePoliza() {
    console.log(this.poliza === 'PO001817');
    if (this.poliza === 'PO001817') {

      this.certificado = '1';
      this.modeda = '1';
      this.inicio = '15/12/2017';
      this.fin = '15/12/2018';
      //this.sucursales = 'BARANQUILLA';
      //this.compania = 'Generales';
      this.ramo = 'AUTOMOVILES';
      this.descripcion = 'Contrato Cuota Parte Automóviles 2017 - 2018';
      this.sumaAsegurada = '3.037.041.614';
      this.sumaDistribucion = '3.037.041.614';
      this.prima = '646.597,00';
      this.ramotecnico = this.ramo;
      this.fecha1 = '4/12/2017';
      this.fecha2 = '';
      this.primaDistribucion = "646.597,00";
      this.moneda = 'COP';

      this.tabla = {
        contrato: 'AUT-CP-2017',
        tramo: '1',
        sumaRetencion: '1.518.520.807',
        sumaCedida: '1.518.520.807',
        primaRetenida: '323.298,50',
        primaCedida: '323.298,50'
      };
      this.identificacion = this.tabla.contrato;
    } else {
      this.certificado = '';
      this.modeda = '' ;
      this.inicio = '';
      this.fin = '';
      this.ramo = '';
      this.descripcion = '';
      this.sumaAsegurada = '';
      this.sumaDistribucion = '';
      this.prima = '';
      this.ramotecnico = '';
      this.fecha1 = '';
      this.fecha2 = '';
      this.primaDistribucion = '';
      this.moneda = '';

      this.tabla = {
        contrato: '',
        tramo: '',
        sumaRetencion: '',
        sumaCedida: '',
        primaRetenida: '',
        primaCedida: ''
      };
      this.identificacion = this.tabla.contrato;
    }
  }

}
