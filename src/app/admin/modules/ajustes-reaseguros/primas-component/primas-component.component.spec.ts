import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimasComponentComponent } from './primas-component.component';

describe('PrimasComponentComponent', () => {
  let component: PrimasComponentComponent;
  let fixture: ComponentFixture<PrimasComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrimasComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimasComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
