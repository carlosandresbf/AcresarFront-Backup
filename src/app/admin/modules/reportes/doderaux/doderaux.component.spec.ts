import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoderauxComponent } from './doderaux.component';

describe('DoderauxComponent', () => {
  let component: DoderauxComponent;
  let fixture: ComponentFixture<DoderauxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoderauxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoderauxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
