import { Observable } from 'rxjs/Observable';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from '../../../../services/session.service';

import { Rsltncntrts } from '../../../../Models/rsltncntrts';
import { HelperService } from '../../../../services/helper.service';
import { NgForm, NgControl } from '@angular/forms';

declare var $: any;

@Component({
  selector: 'app-doderaux',
  templateUrl: './doderaux.component.html',
  styleUrls: ['./doderaux.component.css'],
  providers:[HelperService]
})
export class DoderauxComponent implements OnInit {

  rsltncr: any;
  currency: any;
  rsltnrsgr: any;

  reseasegurador: any;
  corredor: any;
  j: JQuery;

  constructor(
    private router: Router,
    private _service: HelperService,
    private _sl: SessionService
  ) {
  }

  ngOnInit() {

    $('#fInicio').datepicker({
      format: 'dd/mm/yyyy'
    });
    $('#fFin').datepicker({
      format: 'dd/mm/yyyy'
    });
    this._service.getQuery('/rsltncrrdrs').subscribe(
      res => {
        this.rsltncr = res;
      }
    );

    this._service.getQuery('/rsltnrsgrdrs').subscribe(
      res => { this.rsltnrsgr = res; }
    );
    this._service.getQuery('/rsltnmnds').subscribe(
      res => { this.currency = res; }
    );
  }

}
