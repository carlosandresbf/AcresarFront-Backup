import { HelperService } from './../../../../../services/helper.service';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
//import { HelperService } from '../../../../../services/helper.service/HelperService';

@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.component.html',
  styleUrls: ['./reportes.component.css'],
  providers: [HelperService]
})
export class ReportesComponent implements OnInit {

  formReporte: FormGroup;
  estadoData = [
    {id:1, value: 'cotizacion'},
    {id:2, value: 'Renovacion'},
    {id:3, value: 'Orden en Firme'},
    {id:4, value: 'Declinado'}
  ];
  intermedario: any;
  corredores: any;
  reaseguradores: any;

  constructor(
    public _http: HelperService
  ) { }

  ngOnInit() {
    this.createForm();

    this._http.getQuery('/intermediarios').subscribe(
      res => {
        this.intermedario = res;
      }
    );
    this._http.getQuery('/corredores').subscribe(
      res => {
        this.corredores = res;
      }
    );
    this._http.getQuery('/reaseguradoras').subscribe(
      res => {
        this.reaseguradores = res;
      }
    )
  }

  createForm() {
    this.formReporte = new FormGroup({
      intermedario : new FormControl('', Validators.required),
      aseguradora : new FormControl('', Validators.required),
      partcipacionIntermediario : new FormControl('', Validators.required),
      comisionAsegurada : new FormControl('', Validators.required),
      corredor: new FormControl('', Validators.required),
      reasegurador: new FormControl('', Validators.required),
      participacionCorredor: new FormControl('', Validators.required),
      participacionReaseguradores : new FormControl('', Validators.required),
      comisionCorredor: new FormControl('', Validators.required),
      comisionCesion: new FormControl('', Validators.required),
      asegurado: new FormControl('', Validators.required),
      nit: new FormControl('',  Validators.required),
      ramoProducto: new FormControl('',  Validators.required),
      tipoContrato: new  FormControl('', Validators.required),
      primaComercial: new FormControl('', Validators.required),
      primaNeta: new FormControl('', Validators.required),
      inicioVigencia: new FormControl('', Validators.required),
      finVigencia: new FormControl('', Validators.required),
      notaCovertura: new FormControl('', Validators.required),
      estado: new FormControl('', Validators.required),
      nota: new FormControl('', Validators.required),
      modificaciones: new FormControl('', Validators.required)

    });

  }
  guardar() {
    console.log(this.formReporte.value);
  }

}
