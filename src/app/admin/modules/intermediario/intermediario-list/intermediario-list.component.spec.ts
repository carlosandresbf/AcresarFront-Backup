import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntermediarioListComponent } from './intermediario-list.component';

describe('IntermediarioListComponent', () => {
  let component: IntermediarioListComponent;
  let fixture: ComponentFixture<IntermediarioListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntermediarioListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntermediarioListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
