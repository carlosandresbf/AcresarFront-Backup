import { Observable } from 'rxjs/Observable';
import { Component, OnInit } from '@angular/core';
import { HelperService } from '../../../../services/helper.service';

@Component({
  selector: 'app-intermediario-list',
  templateUrl: './intermediario-list.component.html',
  styleUrls: ['./intermediario-list.component.css'],
  providers: [HelperService]
})
export class IntermediarioListComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  ls: Observable<any>;
  modulo: string = 'Listado de Intermediarios';

  constructor(private _ls: HelperService, ) {
    this.ls = this._ls.getQuery('/intermediarios').do(function () {
      setTimeout(function () {
        $("#myData").DataTable({ pagingType: 'full_numbers' }).draw();
        $("#myData").show();
        $(window).resize();
      }, 200);
    });

  }

  ngOnInit() {
  }

}
