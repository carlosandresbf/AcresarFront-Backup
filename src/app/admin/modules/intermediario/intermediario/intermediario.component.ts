import { Component, OnInit } from '@angular/core';
import { RsltnntddsService } from '../../../../services/rsltnntdds.service';
import { HelperService } from '../../../../services/helper.service';
import { RsltncrrdrsService } from '../../../../services/rsltncrrdrs.service';
import { RsltnpssService } from './../../../../services/rsltnpss.service';
import { Router } from "@angular/router";
import { Observable } from 'rxjs';

@Component({
  selector: 'app-intermediario',
  templateUrl: './intermediario.component.html',
  styleUrls: ['./intermediario.component.css'],
  providers: [
    RsltnpssService,
    RsltnntddsService,
    HelperService,
    RsltncrrdrsService
  ]
})
export class IntermediarioComponent implements OnInit {
  modulo: string = "Registrar Intermediario";
  Rsltnpss: Observable<any>;
  Rsltnntdds: Observable<any>;
  autocomplete: JQuery;
  razonsocial: string;
  of: string;
  rl: string = "/intermediarios";
  dataJson: any;
  itemData = { c: '', pc: '', s: '', ca: '', sa: '', cxa: '', ra: '', e: '', cc: '', r: '' };


  constructor(
    private router: Router,
    private _Rsltnpss: RsltnpssService,
    private _Rsltnntdds: RsltnntddsService,
    private service: HelperService
  ) {
  }

  ngOnInit() {
    this.Rsltnpss = this.service.getQuery('/pais');
    this.Rsltnntdds = this.service.getQuery('/entidades');
  }

  create(item) {

    console.log(item);
    this.service.postQuery(item, this.rl).subscribe(
      item => {
        //   console.log(item[0].mensaje);
        alert(item.item.mensaje);
        this.router.navigate(["/admin/intermediario/list"]);
      },
      error => console.log(<any>error)
    );
  }


}
