//mport { HelperService } from './../../../../Cores/Services/helper.service';
import { Component, OnInit, ElementRef } from '@angular/core';
import { FileUploadServiceService } from './../../../../services/file-upload-service.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { HelperService } from '../../../../services/helper.service';

@Component({
  selector: 'app-reacoex-upload',
  templateUrl: './reacoex-upload.component.html',
  styleUrls: ['./reacoex-upload.component.css'],
  providers: [FileUploadServiceService, HelperService]
})

export class ReacoexUploadComponent implements OnInit {
  modulo: string = "Actualización Reacoex";
  dtOptions: DataTables.Settings = {};
  f: FormGroup;
  fileToUpload: File = null;
  filename: string;
  ext: string;
  readyToSend: boolean = false;
  reacodexResaguradores: any;

  public inputFileModel: Array<any> = new Array<any>();
  public inputFileMinimalModel: Array<any> = new Array<any>();
  FileStore: any;

  /* private fileUploader: FileUploadServiceService, private elem: ElementRef */
  constructor(
    private _http: HttpClient,
    private formBuilder: FormBuilder,
    private _upload: FileUploadServiceService,
    private _service: HelperService
  ) {

  }


  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 25
    };

    this.f = this.formBuilder.group({
      inputFile: [null, [Validators.required]],
      typeFileInput: [null, [Validators.required]]
    });

    this._service.getQuery('/reaseguradoras/list/reacoex').subscribe(
      res => {
        this.reacodexResaguradores = res;
      },
      err => {
        console.log(err);
      }
    );

  }

  private handleFileInput(event) {
    this.fileToUpload = event.target.files[0];
    this.filename = this.f.controls['inputFile'].value.split('.');
    this.ext = this.filename[this.filename.length - 1].toLowerCase();
    if (this.ext == 'xls' || this.ext == 'xlsx') {
      this.f.controls['typeFileInput'].setValue(this.ext);
      this.readyToSend = true;
    } else {
      alert("Solo se permiten archivos de excel (xls, xlsx).");
      this.f.reset();
      this.readyToSend = false;
    }

  }

  public onAccept(file: any): void {
    this.FileStore = file;
  }

  public onRemove(file: any): void {
    this.FileStore = '';
  }

  SendFile() {
      const fb: FormData =  new FormData();
      fb.append('fileName', this.FileStore.file, this.FileStore.file.name );

      this._upload.uploadFile(fb).subscribe(data => {
        console.log('4');
      }, error => {
        console.log(error);
      });

    }
  }

