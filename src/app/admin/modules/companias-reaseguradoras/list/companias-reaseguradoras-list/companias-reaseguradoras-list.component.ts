import { Observable } from 'rxjs/Observable';
import { Component, OnInit } from '@angular/core';
import { HelperService } from '../../../../../services/helper.service';

declare var $: any;
@Component({
  selector: 'app-companias-reaseguradoras-list',
  templateUrl: './companias-reaseguradoras-list.component.html',
  styleUrls: ['./companias-reaseguradoras-list.component.css'],
  providers: [HelperService]
})
export class CompaniasReaseguradorasListComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  ls:Observable<any>;
  modulo: string = 'Compañias ';
  constructor(private _ls: HelperService,) {

    this.ls = this._ls.getQuery('/rsltnrsgrdrs').do(function () {
      setTimeout(function () {
        $("#myData").DataTable({ pagingType: 'full_numbers' }).draw();
        $("#myData").show();
        $(window).resize();
      }, 200);
    });

  }

  ngOnInit() {
  }


}
