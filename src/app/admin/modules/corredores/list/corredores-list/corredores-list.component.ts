import { Observable } from 'rxjs/Observable';
import { Component, OnInit } from '@angular/core';
import { HelperService } from '../../../../../services/helper.service';

@Component({
  selector: 'app-corredores-list',
  templateUrl: './corredores-list.component.html',
  styleUrls: ['./corredores-list.component.css'],
  providers: [HelperService]
})
export class CorredoresListComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  ls: Observable<any>;
  modulo: string = 'Listado de Corredores';

  constructor(private _ls: HelperService, ) {
    this.ls = this._ls.getQuery('/rsltncrrdrs').do(function () {
      setTimeout(function () {
        $("#myData").DataTable({ pagingType: 'full_numbers' }).draw();
        $("#myData").show();
        $(window).resize();
      }, 200);
    });

  }

  ngOnInit() {
  }

}
