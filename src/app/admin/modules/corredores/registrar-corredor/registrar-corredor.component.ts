import { RsltnpssService } from "./../../../../services/rsltnpss.service";
import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { RsltnntddsService } from "../../../../services/rsltnntdds.service";
import { HelperService } from "../../../../services/helper.service";
import { RsltncrrdrsService } from "../../../../services/rsltncrrdrs.service";
import { Router } from "@angular/router";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/debounceTime";
import {} from "jquery";
import {} from "daterangepicker";


@Component({
  selector: "app-registrar-corredor",
  templateUrl: "./registrar-corredor.component.html",
  styleUrls: ["./registrar-corredor.component.css"],
  providers: [
    RsltnpssService,
    RsltnntddsService,
    HelperService,
    RsltncrrdrsService
  ]
})

export class RegistrarCorredorComponent   implements OnInit {
  modulo: string = "Registrar Corredores";
  Rsltnpss: Observable<any>;
  Rsltnntdds: Observable<any>;
  autocomplete: JQuery;
  razonsocial: string;
  of: string;
  rl: string = "/rsltncrrdrs";
  dataJson: any;
  lisRequest = false;
  reaseguroData = { c: '', pc: '', s: '', ca: '', sa: '', cxa: '', ra: '', e: '', cc: '', r: ''};

  constructor(
    private _rgstr: RsltncrrdrsService,
    private router: Router,
    private _Rsltnpss: RsltnpssService,
    private _Rsltnntdds: RsltnntddsService,
    private service: HelperService
  ) {
  }

  ngOnInit() {
    this.Rsltnpss = this.service.getQuery('/pais');
    this.Rsltnntdds = this.service.getQuery('/entidades');
  }

  create(item) {

   /* item.r = this.reaseguroData.sa;
    item.com  =  this.reaseguroData.ca;
    item.p = this.reaseguroData.ra;
    item.u = this.reaseguroData.e;*/

    item.r = this.reaseguroData.c;
    item.com = this.reaseguroData.ca;
    item.p = this.reaseguroData.sa;
    item.u = this.reaseguroData.r;

    
console.log(item);
    this.service.postQuery(item, this.rl).subscribe(
      item => {
        //   console.log(item[0].mensaje);
        alert(item.item.mensaje);
        this.router.navigate(["/admin/companiasreaseguradoras/corredor/list"]);
      },
      error => console.log(<any>error)
    );
  }

  consulta(json: any) {
    console.log(json);
    const item = {
      'module': 'corredor',
      'razon': json,
    };

    this.service.postQuery(item, '/razonSocial' ).subscribe(
      res => {
        if (res.length > 0) {
          this.lisRequest = true;
          this.reaseguroData = res;
        }

      },
      err => {
        console.log(err);
      }
    );

  }
  cargar(item: any) {
    this.lisRequest = false;
    this.reaseguroData = item;
  }
}
