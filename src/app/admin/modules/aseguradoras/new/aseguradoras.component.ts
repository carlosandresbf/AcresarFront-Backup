import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RsltnntddsService } from '../../../../services/rsltnntdds.service';
import { HelperService } from '../../../../services/helper.service';
import { RsltncrrdrsService } from '../../../../services/rsltncrrdrs.service';
import { RsltnpssService } from './../../../../services/rsltnpss.service';
import { Router } from "@angular/router";
import { Observable } from 'rxjs';

@Component({
  selector: 'app-aseguradoras',
  templateUrl: './aseguradoras.component.html',
  styleUrls: ['./aseguradoras.component.css'],
  providers: [
    RsltnpssService,
    RsltnntddsService,
    HelperService,
    RsltncrrdrsService
  ]
})
export class AseguradorasComponent implements OnInit {
  modulo: string = 'Registrar Aseguradora';
  type = "";
  Rsltnpss: Observable<any>;
  Rsltnntdds: Observable<any>;
  autocomplete: JQuery;
  razonsocial: string;
  of: string;
  rl: string = "/aseguradoras";
  dataJson: any;
  itemData = { c: '', pc: '', s: '', ca: '', sa: '', cxa: '', ra: '', e: '', cc: '', r: '' };
  constructor(private route: ActivatedRoute,
    private router: Router,
    private _Rsltnpss: RsltnpssService,
    private _Rsltnntdds: RsltnntddsService,
    private service: HelperService) {
    this.type = this.route.snapshot.params.type;
    switch (this.type) {
      case 'Cooperativas':
        this.itemData.e = '3';
       // alert(3);
        break;
      case 'Seguros de vida':
        this.itemData.e = '2';
      //  alert(2);
        break;
      case 'Seguros generales':
        this.itemData.e = '1';
        //alert(1);
        break;
        
    }
  }

  ngOnInit() {
    this.Rsltnpss = this.service.getQuery('/pais');
    this.Rsltnntdds = this.service.getQuery('/entidades');

  }

  create(item) {

    console.log(item);
    this.service.postQuery(item, this.rl).subscribe(
      item => {
        //   console.log(item[0].mensaje);
        alert(item.item.mensaje);
        this.router.navigate(["/admin/companiasreaseguradoras/aseguradoras/list"]);
      },
      error => console.log(<any>error)
    );
  }



}
