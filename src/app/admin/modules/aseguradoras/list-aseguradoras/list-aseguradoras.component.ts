import { Observable } from 'rxjs/Observable';
import { Component, OnInit } from '@angular/core';
import { HelperService } from '../../../../services/helper.service';

@Component({
  selector: 'app-list-aseguradoras',
  templateUrl: './list-aseguradoras.component.html',
  styleUrls: ['./list-aseguradoras.component.css'],
  providers: [HelperService]
})
export class ListAseguradorasComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  ls: Observable<any>;
  modulo: string = 'Listado de Aseguradoras';


  constructor(private _ls: HelperService, ) {
    this.ls = this._ls.getQuery('/aseguradoras').do(function () {
      setTimeout(function () {
        $("#myData").DataTable({ pagingType: 'full_numbers' }).draw();
        $("#myData").show();
        $(window).resize();
      }, 200);
    });

  }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers'
    };
  }

}
