import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";

import { LocalStorageService, SessionStorageService } from "ngx-webstorage";
import { } from "jquery";
import { Observable } from "rxjs/Observable";
import { Router } from "@angular/router";
import { HelperService } from "../../../services/helper.service";
import { RsltnntddsService } from "../../../services/rsltnntdds.service";
import { } from 'jquery';
import { } from 'daterangepicker';
import * as moment from 'moment';

declare var $: any;

@Component({
  selector: "app-ramos",
  templateUrl: "./ramos.component.html",
  styleUrls: ["./ramos.component.css"],
  providers: [HelperService, RsltnntddsService]
})
export class RamosComponent implements OnInit {
  calendar: JQuery;
  ls: Observable<any>;
  codSuper: Observable<any>;
  namSuper: Observable<any>;
  modulo: string = "Listado de Ramos";
  Rsltnntdds: Observable<any>;
  ready: boolean = false;
  subRamo = [];
  cobertura = [];
  producto = [];
  abreviatura = [];
  codProducto = [];
  it: JQuery;
  myForm: FormGroup;
  list: any;
  rt = '/ramos';
  datatableshow = false;

  constructor(
    private _ls: HelperService,
    private router: Router,
    private _Rsltnntdds: RsltnntddsService
  ) {
    
  

  }

  reloadTable(){
    $("#myData").DataTable().destroy();
    this._ls.getQuery(this.rt).subscribe(
      res => {
        this.list = res;
        console.log(this.list);
        const st = setTimeout(function () {
          $("#myData")
            .DataTable()
            .draw();
          $("#myData").fadeIn();
          $(window).resize();
          clearTimeout(st);
        }, 3000);

      }
    );
  }
  loadDataTable(){

    this._ls.getQuery(this.rt).subscribe(
      res => {
        this.list = res;

        const st = setTimeout(function () {
          $("#myData")
            .DataTable()
            .draw();
          $("#myData").fadeIn();
          $(window).resize();
          clearTimeout(st);
        }, 1000);

      }
    );

  }


  ngOnInit() {

    this.loadDataTable();

    this.createForm();
    this.Rsltnntdds = this._Rsltnntdds.getAll();
    this.codSuper = this._ls.getQuery("/ramos/list/superintendencia?order=code");
    this.namSuper = this._ls.getQuery("/ramos/list/superintendencia");
    $("#codigoSuper").on("change", function () {
      $("#ramoSuper").val($(this).val());
    });

    $("#ramoSuper").on("change", function () {
      $("#codigoSuper").val($(this).val());
    });

    $(".addnewSrCv").on("click", function () {
      var target = $(".target2");
      var cantRows = target.data("cantRows");
      var sbrm = target.data("cantSbrm");
      var cvr = target.data("cantCvr");
      var sbrmItem = target.data("cantSbrmItm");
      var cvrItem = target.data("cantCvrItm");
      var newItemSbr;
      var newItemCbr;

      if ($(this).attr("id") == "addNewSr") {
        sbrmItem++;
        target.data("cantSbrmItm", sbrmItem);
      } else {
        cvrItem++;
        target.data("cantCvrItm", cvrItem);
      }

      if (sbrmItem > sbrm || cvrItem > cvr) {
        sbrm++;
        target.data("cantSbrm", sbrm);
        cvr++;
        target.data("cantCvr", cvr);
      }

      if (sbrm > cantRows || cvr > cantRows) {
        newItemSbr = $("<div>", {
          class: "col-xs-6 colSr" + sbrm
        });

        newItemCbr = $("<div>", {
          class: "col-xs-6 colCov" + sbrm
        });

        cantRows++;
        target.data("cantRows", cantRows);
        $("<div>", {
          class: "row row" + cantRows
        })
          .append(newItemSbr, newItemCbr)
          .hide()
          .appendTo(".target2")
          .fadeIn("slow");
      }

      if ($(this).attr("id") == "addNewSr") {
        $("<div>", {
          class: "form-group"
        })
          .append(
            $("<label>", {
              for: "subRamo" + sbrmItem,
              text: "Sub Ramo"
            }),
            $("<input>", {
              type: "text",
              class: "form-control",
              name: "subRamo" + sbrmItem,
              //  '[(ngModel)]': 'subRamo.subRamo' + sbrmItem,
              placeholder: "Sub Ramo"
            }).attr('name', 'subRamo')
          )
          .hide()
          .appendTo(".colSr" + sbrmItem)
          .fadeIn("slow");
      } else {
        $("<div>", {
          class: "form-group"
        })
          .append(
            $("<label>", {
              for: "cobertura" + cvrItem,
              text: "Cobertura"
            }),
            $("<input>", {
              type: "text",
              class: "form-control",
              name: "cobertura" + cvrItem,
              // '[(ngModel)]': 'cobertura.cobertura' + cvrItem,
              placeholder: "Cobertura"
            }).attr('name', 'cobertura')
          )
          .hide()
          .appendTo(".colCov" + cvrItem)
          .fadeIn("slow");
      }
    });

    $("#addNew").on("click", function () {
      var target = $(".target");
      var cantRows = target.data("cantRows");
      cantRows++;
      target.data("cantRows", cantRows);

      var my = "";
      my += ' <div class="row">';
      my += '<div class="col-xs-3 ">';
      my += '<div class="form-group">';
      my += '<label for="codProd' + cantRows + '">Cod. Producto</label>';
      my +=
        '<input type="text" class="form-control codproducto" name="codProd' +
        cantRows +
        '" [(ngModel)]= "producto.codProd' +
        cantRows +
        '" placeholder="Cod. producto">';
      my += "</div>";
      my += "</div>";
      my += '<div class="col-xs-3 ">';
      my += '<div class="form-group">';
      my += '<label for="abrevProd' + cantRows + '">Abreviatura</label>';
      my +=
        '<input type="text" class="form-control abreviatura" name="abrevProd' +
        cantRows +
        '" [(ngModel)]= "producto.abrevProd' +
        cantRows +
        '"  placeholder="Abreviatura ">';
      my += "</div>";
      my += "</div>";
      my += '<div class="col-xs-6 ">';
      my += '<div class="form-group">';
      my += '<label for="producto' + cantRows + '">Producto</label>';
      my +=
        '<input type="text" class="form-control producto"  name="producto' +
        cantRows +
        '" [(ngModel)]= "producto.producto' +
        cantRows +
        '"  placeholder="Producto">';
      my += "</div>";
      my += "</div>";
      my += "</div>";
      jQuery(".target").append(my);
    });

    this.calendar = $("#calendar").datepicker({
      format: "yyyy",
      viewMode: "years",
      minViewMode: "years"
    });

    $("#calendar").on("changeDate", function (ev) {
      $(this).datepicker("hide");
    });
  }

  createForm() {
    this.myForm = new FormGroup({
      entidad: new FormControl("", Validators.required),
      ramoSuper: new FormControl("", Validators.required),
      codigoSuper: new FormControl("", Validators.required),
      codigoRamoTecnico: new FormControl("", Validators.required),
      abreviatura: new FormControl("", Validators.required),
      ramoTecnico: new FormControl("", Validators.required)
    });
  }

  sinconiza(i: string) {
    if (i == 'codigo') {
      this.myForm.controls.ramoSuper.setValue(this.myForm.controls.codigoSuper.get);

    }
    if (i == 'ramo') {
      this.myForm.controls.codigoSuper.setValue(this.myForm.controls.ramoSuper.get);
    }
  }
  guardar() {

    $("#myData").fadeOut();
    const subRamos = $('input[name=subRamo]');
    const cobertura = $('input[name=cobertura]');
    const codproducto = $('.codproducto');
    const abreviatura = $('.abreviatura');
    const producto = $('.producto');

    for (let i = 0; i <= subRamos.length - 1; i++) {
      let e = subRamos[i];
      this.subRamo.push(e.value);
    }

    for (let i = 0; i <= cobertura.length - 1; i++) {
      let e = cobertura[i];
      this.cobertura.push(e.value);
    }

    for (let i = 0; i <= producto.length - 1; i++) {
      let p = producto[i];
      let c = codproducto[i];
      let a = abreviatura[i];
      const json = {
        'producto': p.value,
        'abreviatura': a.value,
        'codProducto': c.value
      };

      this.producto.push(json);
    }



    let dataJson = this.myForm.value;
    dataJson['coberturas'] = this.cobertura;
    dataJson['subRamos'] = this.subRamo;
    dataJson['producto'] = this.producto;
    
    

   this._ls.postQuery(dataJson, this.rt).subscribe(
      res => {
      
        this.myForm.reset();
        $('#cerrar').click();
     //   window.location.reload(); 
    this.reloadTable();
        
      },
      err => {
        this.myForm.reset();
        $('#cerrar').click();
        console.log(err);
      }
    );
  }
}
