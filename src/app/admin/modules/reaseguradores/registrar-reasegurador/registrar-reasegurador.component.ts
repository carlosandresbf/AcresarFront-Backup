import { RsltngncsclfcdrsService } from "./../../../../services/rsltngncsclfcdrs.service";
import { RsltnpssService } from "./../../../../services/rsltnpss.service";
import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/debounceTime";
import { FormGroup, FormControl } from "@angular/forms";
import { RsltnntddsService } from "../../../../services/rsltnntdds.service";
import { HelperService } from "../../../../services/helper.service";
import { RsltnrsgrdrsService } from "../../../../services/rsltnrsgrdrs.service";
import { Router } from "@angular/router";


import {} from "jquery";
import {} from "daterangepicker";

@Component({
  selector: "app-registrar-reasegurador",
  templateUrl: "./registrar-reasegurador.component.html",
  styleUrls: ["./registrar-reasegurador.component.css"],
  providers: [
    RsltnpssService,
    RsltngncsclfcdrsService,
    RsltnntddsService,
    HelperService,
    RsltnrsgrdrsService
  ]
})
export class RegistrarReaseguradorComponent implements OnInit {
  Rsltnpss: Observable<any>;
  Rsltngncsclfcdrs: Observable<any>;
  Rsltnntdds: Observable<any>;
  Rsltncrgrsgrdrsrcx: Observable<any>;
  slgrsgrdrsrcx;
  autocomplete: JQuery;
  razonsocial: string;
  of: string;
  rl: string = "/rsltnrsgrdrs";
  r: string = "";
  modulo: string = "Registrar Reaseguradores";
  reaseguroData = { act: '', c: '', pc: '', s: '',ca:'',sa:'',cxa:''};
  lisRequest = false;


  constructor(
    private _rgstr: RsltnrsgrdrsService,
    private router: Router,
    private _Rsltnpss: RsltnpssService,
    private _Rsltngncsclfcdrs: RsltngncsclfcdrsService,
    private _Rsltnntdds: RsltnntddsService,
    private service: HelperService
  ) {
    this.Rsltncrgrsgrdrsrcx = this.service.getQuery("/rsltncrgrsgrdrsrcx");
  }

  ngOnInit() {
    this.Rsltnpss = this.service.getQuery('/pais');
    this.Rsltngncsclfcdrs = this.service.getQuery('/agenciacalificadora');
    this.Rsltnntdds = this.service.getQuery('/entidades');

    /*
    this.slgrsgrdrsrcx = this.service.search_word('/rsltncrgrsgrdrsrcx','');

    this.autocomplete = jQuery('#razonsocial').typeahead({source: this.slgrsgrdrsrcx });
  */
  }

  create(item) {
    item.ag = this.reaseguroData.cxa;
    item.p = this.reaseguroData.sa;
    item.r = this.reaseguroData.ca;
    item.act = this.reaseguroData.act;

    this.service.postQuery(item, this.rl).subscribe(
      item => {
        //   console.log(item[0].mensaje);
        alert(item.item.mensaje);
        this.router.navigate(["/admin/companiasreaseguradoras"]);
      },
      error => console.log(<any>error)
    );
  }

  consulta(json:any) {
    console.log(json);
    const item = {
      'module': 'reaseguradores',
      'razon': json,
    };

    this.service.postQuery(item, '/razonSocial' ).subscribe(
      res => {
        this.lisRequest = true;
        this.reaseguroData = res;
      },
      err => {
        console.log(err);
      }
    );
    console.log(this.r);
  }
  cargar(item: any) {
    console.log(item);
    this.lisRequest = false;
    this.reaseguroData = item;
  }
}
