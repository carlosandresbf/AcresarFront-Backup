import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CumulosListComponent } from './cumulos-list.component';

describe('CumulosListComponent', () => {
  let component: CumulosListComponent;
  let fixture: ComponentFixture<CumulosListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CumulosListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CumulosListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
