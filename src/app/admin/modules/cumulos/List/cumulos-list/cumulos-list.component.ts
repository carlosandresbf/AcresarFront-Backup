import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cumulos-list',
  templateUrl: './cumulos-list.component.html',
  styleUrls: ['./cumulos-list.component.css']
})
export class CumulosListComponent implements OnInit {

  modulo: string = 'Cúmulos';
  constructor() { }

  ngOnInit() {
  }

}
