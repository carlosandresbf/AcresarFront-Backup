import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lineas-contratos-list',
  templateUrl: './lineas-contratos-list.component.html',
  styleUrls: ['./lineas-contratos-list.component.css']
})
export class LineasContratosListComponent implements OnInit {

  modulo: string = 'Lineas de Contratos';
  constructor() { }

  ngOnInit() {
  }

}
