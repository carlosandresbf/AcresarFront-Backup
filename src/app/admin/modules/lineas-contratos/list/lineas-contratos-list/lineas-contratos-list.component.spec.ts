import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LineasContratosListComponent } from './lineas-contratos-list.component';

describe('LineasContratosListComponent', () => {
  let component: LineasContratosListComponent;
  let fixture: ComponentFixture<LineasContratosListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LineasContratosListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LineasContratosListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
