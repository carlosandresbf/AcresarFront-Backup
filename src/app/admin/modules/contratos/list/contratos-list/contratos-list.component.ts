import { Component, OnInit } from '@angular/core';
import { } from 'jquery';
import { } from 'bootstrap-datepicker';
import { } from 'daterangepicker';
import * as moment from 'moment';
import { Rsltncntrts } from '../../../../../models/rsltncntrts';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { HelperService } from '../../../../../services/helper.service';
declare var $: any;
@Component({
  selector: 'app-contratos-list',
  templateUrl: './contratos-list.component.html',
  styleUrls: ['./contratos-list.component.css'],
  providers: [HelperService]
})
export class ContratosListComponent implements OnInit {

  calendar: JQuery;
  ls: Observable<any>;
  modulo: string = 'Contratos';
  item: Rsltncntrts = new Rsltncntrts('', '', '', '', '', '', '', '', '');
  ready: boolean = false;

  constructor(
    private _ls: HelperService,
    private router: Router
  ) {

    this.ls = this._ls.getQuery('/rsltncntrts').do(function () {
      setTimeout(function () {
        $("#myData").DataTable({ pagingType: 'full_numbers' }).draw();
        $("#myData").fadeIn();
        $(window).resize();
      }, 200);
    });
  }

  ngRedirect() {

    

    this.item.r = jQuery('#calendar').val().toString();
    this.item.e = jQuery('#calendar').val().toString();
    localStorage.setItem('rsltntmpcntrt', JSON.stringify(this.item));
    const data = {
      'idContrato': this.item.r,
      'anoContratual': this.item.e,
      'tipoContrato' : this.item.a2
    };
    this._ls.postQuery(data, '').subscribe(
      res => {
        console.log(res);
      },
      err => {
        console.log(err);
      }
    );
    this.caseRuting(this.item.a2);

  }

  ngOnInit() {
    $('#calendar').datepicker({
      format: 'yyyy'
    });
    localStorage.removeItem('rsltntmpcntrt');
    /*
    this.calendar = jQuery('#calendar').datepicker({
      format: 'yyyy',
      viewMode: "years",
      minViewMode: "years"
    });
    jQuery('#calendar').on('changeDate', function (ev) {
      $(this).datepicker('hide');
    });*/
  }

  caseRuting(ruta: string ){

    switch ( ruta ) {
      case 'Cuota Parte':
        this.router.navigateByUrl('admin/contratos/automaticos/proporcionales/cuota-aparte');
        break;
      case 'Excedente':
        this.router.navigateByUrl('admin/contratos/automaticos/proporcionales/excedente');
        break;
      case 'FACOB':
        this.router.navigateByUrl('admin/contratos/automaticos/proporcionales/facob');
        break;
      case 'Operativo WXL':
        this.router.navigateByUrl('admin/contratos/automaticos/noproporcionales/operativowxl');
        break;
      case 'Catastrofico XL':
        this.router.navigateByUrl('admin/contratos/automaticos/noproporcionales/catastroficoxl');
        break;
      case 'Stop Loss':
        this.router.navigateByUrl('admin/contratos/automaticos/noproporcionales/stoploss');
        break;
      case 'Tent Plan':
        this.router.navigateByUrl('admin/contratos/automaticos/noproporcionales/tentplan');
        break;
      case 'Facultativo':
        this.router.navigateByUrl('admin/contratos/facultativos/proporcionales/facultativo');
        break;
      case 'Facultativo NP':
        this.router.navigateByUrl('admin/contratos/facultativos/noproporcionales/facultativo');
        break;
    }
  }

}
