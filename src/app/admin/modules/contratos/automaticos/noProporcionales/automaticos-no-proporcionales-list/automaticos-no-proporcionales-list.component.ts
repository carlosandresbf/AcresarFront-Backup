import { Component, OnInit } from '@angular/core';
import { } from 'jquery';
import { } from 'morris.js';
import { } from 'jquery-knob';
import { } from 'bootstrap-datepicker';
import { } from 'jqueryui';
import { } from 'daterangepicker';
import { } from 'jquery.slimscroll';
import * as moment from 'moment';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { HelperService } from '../../../../../../services/helper.service';


@Component({
  selector: 'app-automaticos-no-proporcionales-list',
  templateUrl: './automaticos-no-proporcionales-list.component.html',
  styleUrls: ['./automaticos-no-proporcionales-list.component.css'],
  providers: [HelperService]
})
export class AutomaticosNoProporcionalesListComponent implements OnInit {
  calendar: JQuery;
  ls: Observable<any>;
  ready: boolean = false;
  items = { type: 'Automaticos', subtype: 'No Proporcionales' };
  modulo: string = 'Automáticos No Proporcionales';
  constructor(private _ls: HelperService, private router: Router) {
    this.ls = this._ls.postQuery(this.items, '/rsltncntrts/filter').do(function () {
      setTimeout(function () {
        $("#myData").DataTable({ pagingType: 'full_numbers' }).draw();
        $("#myData").show();
        $(window).resize();
      }, 200);
    });
  }


  ngOnInit() {
  }

}
