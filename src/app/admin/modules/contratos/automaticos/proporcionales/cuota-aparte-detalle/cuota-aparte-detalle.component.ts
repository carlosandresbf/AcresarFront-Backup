import { Observable } from "rxjs/Observable";
import { Component, OnInit, OnDestroy } from "@angular/core";
import { Router } from "@angular/router";
import { } from "jquery";
import { } from "daterangepicker";
import * as moment from "moment";
import { SessionService } from "../../../../../../services/session.service";

import { Rsltncntrts } from "../../../../../../Models/rsltncntrts";
import { HelperService } from "../../../../../../services/helper.service";
import { NgForm, NgControl } from "@angular/forms";

@Component({
  selector: "app-cuota-aparte-detalle",
  templateUrl: "./cuota-aparte-detalle.component.html",
  styleUrls: ["./cuota-aparte-detalle.component.css"],
  providers: [HelperService]
})
export class CuotaAparteDetalleComponent implements OnInit {
  datajsonNominas: any = [];
  rsltncr: Observable<any>;
  rsltnrsgr: Observable<any>;
  cmsn: JQuery;
  currency: Observable<any>;
  _ls_trasp_tip:any;
  _ls_trasp_cuen: any;
  frmValues = {
    dtcr: "",
    dtrsg: "",
    dtpp: "",
    dtcu: "",
    dtpcm: "",
    dtgst: "",
    dtipc: "",
    dtir: "",
    dtap: "",
    dtcts: "",
    dtbrok: "",
    dtmdfj: "",
    dtmdcmsl: "",
    dtmdprmxcm1: "",
    dtmdprmncm1: "",
    dtmdprmxsn1: "",
    dtmdprmnsn1: "",
    dtmdcmslsc: "",
    dtmdsccm1: "",
    dtmdscsn1: "",
    dtmd2mn: "",
    dtmd2prd: "",
    dtmd2pt: "",
    dtmd2prtd: "",
    dtmd2pild: "",
    dtmd2rsv: "",
    dttps: "",
    dtmdtrsp: "",
    dtmdtrspcnts: ""
  };
  modulo: string = "Detalle por Reasegurador - Cuota Parte";
  public form: any = {
    cartera: "",
    depositoRetenido: '',
    comision: "",
    corredor: "",
    deposito: {
      PorcentajeR: ""
    },
    traspasoCartera: {
      Cuenta: '',
      traspaso: ''
    },
    ModelComision: {
      sobrecomisionMaxCheck: false,
      sobrecomisionMaxVal: "",
      sobrecomisionMinCheck: false,
      sobrecomisionMinVal: "",
      siniestralidadMaxCheck: false,
      siniestralidadMaxVal: "",
      siniestralidadMinCheck: false,
      siniestralidadMinVal: "",
      valueFija: "",
      ComVal: "",
      SinVal: ""
    }
  };
  private valorComision: string;
  comisionArray = { fija: false, provisional: false, escalonada: false };
  constructor(
    private router: Router,
    private _service: HelperService,
    private _sl: SessionService
  ) {
    this.rsltncr = _service.getQuery("/rsltncrrdrs");
    this.rsltnrsgr = _service.getQuery("/rsltnrsgrdrs");
    this.currency = _service.getQuery("/monedas");
    this._ls_trasp_tip = _service.getQuery("/traspasocartera/tipos");
    this._ls_trasp_cuen = _service.getQuery("/traspasocartera/cuentas");
    if (sessionStorage.getItem("dtcntrcp") === null) {
      this.frmValues = {
        dtcr: "",
        dtrsg: "",
        dtpp: "",
        dtpcm: "",
        dtcu: "",
        dtgst: "",
        dtipc: "",
        dtir: "",
        dtap: "",
        dtcts: "",
        dtbrok: "",
        dtmdfj: "",
        dtmdcmsl: "",
        dtmdprmxcm1: "",
        dtmdprmncm1: "",
        dtmdprmxsn1: "",
        dtmdprmnsn1: "",
        dtmdcmslsc: "",
        dtmdsccm1: "",
        dtmdscsn1: "",
        dtmd2mn: "",
        dtmd2prd: "",
        dtmd2pt: "",
        dtmd2prtd: "",
        dtmd2pild: "",
        dtmd2rsv: "",
        dttps: "",
        dtmdtrsp: "",
        dtmdtrspcnts: ""
      };
    } else {
      this.frmValues = JSON.parse(sessionStorage.getItem("dtcntrcp"));
    }
  }

  ngOnInit() {
    this.frmValues.dtcr = "";
    this.cmsn = $("input[name=dtmdcmsl]").on("click", function () {
      $("#dtmdfj")
        .val($("#" + $(this).val()).val())
        .change()
        .trigger("input");
      $("#dtpcm")
        .val($("#dtmdfj").val())
        .change()
        .trigger("input");
    });

    const condicioneContrato = this._sl.getData("codicioneContrato");
    if (condicioneContrato != null) {
      this.datajsonNominas.push(condicioneContrato);
    }
  }
  sendModalCom() {
    if (this.form.ModelComision.sobrecomisionMaxCheck == true) {
      this.form.comision = this.form.ModelComision.sobrecomisionMaxVal;
    }
    if (this.form.ModelComision.sobrecomisionMinCheck == true) {
      this.form.comision = this.form.ModelComision.sobrecomisionMinVal;
    }
    if (this.form.ModelComision.siniestralidadMaxCheck == true) {
      this.form.comision = this.form.ModelComision.siniestralidadMaxVal;
    }
    if (this.form.ModelComision.siniestralidadMinCheck == true) {
      this.form.comision = this.form.ModelComision.siniestralidadMinVal;
    }
    if (this.valorComision == "fija") {
      this.form.comision = this.form.ModelComision.valueFija;
    }
  }

  onClickComision(key: any) {
    this.valorComision = key;
    let data = Object.keys(this.comisionArray);
    data.forEach(element => {
      if (element == key) {
        this.comisionArray[element] = true;
      } else {
        this.comisionArray[element] = false;
      }
    });
  }

  sendModalDep(item) {
    this.form.depositoRetenido = this.form.deposito.PorcentajeR;
  }

  sendModalTra(item) {
    this.form.cartera = this.form.traspasoCartera.traspaso;
  }
  //agregar
  addComision() {
    this.datajsonNominas.push(this.form);

    this.form = {
      cartera: "",
      depositoRetenido: "",
      comision: "",
      corredor: "",
      deposito: {
        PorcentajeR: ""
      },
      traspasoCartera: {
        Cuenta: '',
        traspaso: ''
      },
      ModelComision: {
        sobrecomisionMaxCheck: false,
        sobrecomisionMaxVal: "",
        sobrecomisionMinCheck: false,
        sobrecomisionMinVal: "",
        siniestralidadMaxCheck: false,
        siniestralidadMaxVal: "",
        siniestralidadMinCheck: false,
        siniestralidadMinVal: "",
        valueFija: ""
      }
    };
  }

  valiarParticipacion() {
    let suma = 0;
    if (this.datajsonNominas != null) {
      for (let i = 0; i <= Object.keys(this.datajsonNominas).length - 1; i++) {
        let item = this.datajsonNominas[i];
        suma = suma + parseInt(item['participacion']);
        console.log('suma', suma);
        if (suma === 100) {
          alert("Participacion debe ser  igual al 100% ");
        }
      }
    }

    if (this.form.participacion >= 101) {
      if (suma === 0) {
        alert("Participacion debe ser menor o igual al 100% ");
      } else {
        const ei = this.form.participacion + suma;
        if (suma > 100) {
          alert('Partición entre comisionistas no puede ser superior al 100%');
        }
      }
    }
    console.log(this.form.participacion);
  }
  //guardar
  guardar() {
    if (this.form.participacion != null && this.form.participacion <= 100) {
      this.router.navigate([
        "/admin/contratos/automaticos/proporcionales/cuota-aparte"
      ]);
      this.datajsonNominas.push(this.form);
      sessionStorage.setItem("comision", JSON.stringify(this.datajsonNominas));
    } else {
      this.router.navigate([
        "/admin/contratos/automaticos/proporcionales/cuota-aparte"
      ]);
    }
  }

  create(item) {
    if (confirm("Esta seguro de guardar los cambios?")) {
      sessionStorage.setItem("dtcntrcp", JSON.stringify(item));
      console.log(sessionStorage.getItem("dtcntrcp"));
      //   this.router.navigate(['admin/contratos/automaticos/proporcionales/cuota-aparte']);
    } else {
      return false;
    }
  }
}
