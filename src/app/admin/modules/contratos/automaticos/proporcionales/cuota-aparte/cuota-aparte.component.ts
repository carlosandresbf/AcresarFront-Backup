
import { Observable } from 'rxjs/Observable';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';


import { Rsltncntrts } from '../../../../../../Models/rsltncntrts';
import { HelperService } from '../../../../../../services/helper.service';
import { NgForm, NgControl } from '@angular/forms';


declare var $: any;
const now = new Date();

@Component({
  selector: 'app-cuota-aparte',
  templateUrl: './cuota-aparte.component.html',
  styleUrls: ['./cuota-aparte.component.css'],
  providers: [HelperService]
})

export class CuotaAparteComponent implements OnInit {
  calendarfi: JQuery;
  calendarff: JQuery;
  number: JQuery;
  item = { c: '', e: '', r: '' };
  frmValues = { cd: '', d: '', fi: '', ff: '', mn: '', s: '', o: '', sl1: '', cs1: '', re1: '', sl2: '', cs2: '', re2: '', sl3: '', cs3: '', re4: '' };
  d = '';
  cod = '';
  currency: Observable<any>;
  rl: string = '/rsltncntrts';
  modulo: string = 'Cuota Parte';
  selectMoneda: any;
  sumaComision = '';

  date: {year: number, month: number};
  model;

  cuotaParteForm: FormGroup;

  constructor(
    private router: Router,
    private _currency: HelperService,
    private service: HelperService,
    private fb: FormBuilder
  ) {
    this.currency = _currency.getQuery('/monedas');
    if (localStorage.getItem('rsltntmpcntrt') === null) {
      this.item.e = '';
      this.item.r = '';
      this.item.c = '';

    } else {
      this.item = JSON.parse(localStorage.getItem('rsltntmpcntrt'));
      this.item.e = this.item.r;
      this.cod = this.item.c + ' - ' + this.item.r;
  }


    if (sessionStorage.getItem('cntrt') === null) {
      this.frmValues = { cd: '', d: '', fi: '', ff: '', mn: '', s: '', o: '', sl1: '', cs1: '', re1: '', sl2: '', cs2: '', re2: '', sl3: '', cs3: '', re4: '' };
    } else {
      this.frmValues = JSON.parse(sessionStorage.getItem('cntrt'));
      // console.log(this.frmValues);
      // this.d = this.frmValues.d;
    }

  }

  ngOnInit() {

    this.createForm();
    if (sessionStorage.getItem('comision') != null) {
      const comisionLocal = JSON.parse(sessionStorage.getItem('comision'));
      if (comisionLocal.length > 0) {
        this.formLoad();
      }
    }


  }

  selectToday() {
    this.model = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};
  }
  comisionItem(item: any) {
    item = JSON.parse(item);
    if (item.length > 0) {
      for (let i = 0; i <= (item.length - 1); i++) {
        const data = item[i];
        console.log(data);
        if (data != null) {
          this.sumaComision = this.sumaComision + parseInt(data['participacion']);
        }

      }
    }
  }
  formLoad() {

    const form = JSON.parse(sessionStorage.getItem('formCuotaP'));
    this.cuotaParteForm.controls.codigocontrato.setValue(form['codigocontrato']);
    this.cuotaParteForm.controls.descripcion.setValue(form['descripcion']);
    this.cuotaParteForm.controls.fechaInicio.setValue(form['fechaInicio']);
    this.cuotaParteForm.controls.fechaFin.setValue(form['fechaFin']);
    this.cuotaParteForm.controls.moneda.setValue(form['moneda']);
    this.selectMoneda = form['moneda'];
    this.cuotaParteForm.controls.siniestroContrato.setValue(form['moneda']);
    this.cuotaParteForm.controls.observacion.setValue(form['observacion']);


    const tb1 = this.cuotaParteForm.controls.tb1.value;

    if (tb1 !== '' || sessionStorage.getItem('tb') === 'fb1') {
      const tb1Data = form['tb1'];

      const tb1 = this.cuotaParteForm.controls.tb1;
      var item ={};
      item = JSON.parse(sessionStorage.getItem('comision'));
      this.comisionItem(sessionStorage.getItem('comision'));

      tb1.setValue(
        {
          contrato: new FormControl('1'),
          sumaLimite: tb1Data['sumaLimite'],
          secion: tb1Data['secion'],
          reas: this.sumaComision.toString(),
          consiliacion: item
        }
      );
    }

    const tb2 =  this.cuotaParteForm.controls.tb2.value;
    if (tb2 === 'tb2' || sessionStorage.getItem('tb') === 'tb2') {
      const tb1Data = form['tb2'];
      console.log(tb1Data);

      const tb = this.cuotaParteForm.controls.tb2;
      var item = {};
      item = JSON.parse(sessionStorage.getItem('comision'));
      this.comisionItem(sessionStorage.getItem('comision'));
      tb.setValue(
        {
          contrato2: new FormControl('1'),
          sumaLimite2: tb1Data['sumaLimite2'],
          secion2: tb1Data['secion2'],
          reas2: this.sumaComision.toString(),
          consiliacion2: item
        }
      );

    }

    const tb3 =  this.cuotaParteForm.controls.tb3.value;
    if (tb3 === 'tb3' || sessionStorage.getItem('tb') === 'tb3') {
      const tb1Data = form['tb3'];

      const tb = this.cuotaParteForm.controls.tb3;
      var item = {};
      item = JSON.parse(sessionStorage.getItem('comision'));
      this.comisionItem(sessionStorage.getItem('comision'));

      tb3.setValue(
        {
          contrato3: new FormControl('1'),
          sumaLimite3: tb1Data['sumaLimite3'],
          secion3: tb1Data['secion3'],
          reas3: this.sumaComision.toString(),
          consiliacion3: item
        }
      );

    }
  }

  createForm() {
    this.cuotaParteForm = new FormGroup({
      codigocontrato: new FormControl('', Validators.required),
      descripcion: new FormControl('', Validators.required),
      fechaInicio: new FormControl('', Validators.required),
      fechaFin: new FormControl('', Validators.required),
      moneda: new FormControl('', Validators.required),
      siniestroContrato: new FormControl('', Validators.required),
      observacion: new FormControl('', Validators.required),
      tb1: new FormGroup(
        {
          contrato: new FormControl('1'),
          sumaLimite: new FormControl(''),
          secion: new FormControl(''),
          reas: new FormControl(''),
          consiliacion: new FormControl('')
        }
      ),
      tb2: new FormGroup(
        {
          contrato2: new FormControl('2'),
          sumaLimite2: new FormControl(''),
          secion2: new FormControl(''),
          reas2: new FormControl(''),
          consiliacion2: new FormControl('')
        }
      ),
      tb3: new FormGroup(
        {
          contrato3: new FormControl('3'),
          sumaLimite3: new FormControl(''),
          secion3: new FormControl(''),
          reas3: new FormControl(''),
          consiliacion3: new FormControl('')
        }
      )
    });
  }



  goDetail(value: string) {
    sessionStorage.clear();
    sessionStorage.setItem('tb', value);
    sessionStorage.setItem('formCuotaP', JSON.stringify(this.cuotaParteForm.value));
    this.router.navigate(['admin/contratos/automaticos/proporcionales/cuota-aparte/detalle']);
  }


  create(item) {
    this.service.postQuery(item, this.rl)
      .subscribe(
        res => {
          //   console.log(item[0].mensaje);
          alert(res.item.mensaje);
          this.router.navigate(['/admin/contratos']);
        },
        error => console.log(<any>error));
  }

  fecha(item: string) {

    if (item == 'fini') {
      this.cuotaParteForm.controls.fechaInicio.setValue($('#' + item).val());
    }
    if (item == 'ffin') {
      this.cuotaParteForm.controls.fechaInicio.setValue($('#' + item).val());
    }
  }

  submitForm() {
    this.cuotaParteForm.controls.codigocontrato.setValue($('#codigocontrato').val());
    console.log(this.cuotaParteForm.value);
    this.service.postQuery(this.cuotaParteForm.value, '/contratos/automaticos/proporcionales/cuotaparte').subscribe(
      res => {
        sessionStorage.clear();
        this.cuotaParteForm.reset();
      },
      err => {

      }
    );
  }

}
