import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorredoresListComponent } from './corredores-list.component';

describe('CorredoresListComponent', () => {
  let component: CorredoresListComponent;
  let fixture: ComponentFixture<CorredoresListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorredoresListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorredoresListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
