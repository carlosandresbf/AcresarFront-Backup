import { Component, OnInit } from '@angular/core';
import { RsltnpssService } from './../../../../../../services/rsltnpss.service';
import { FormGroup, FormControl } from '@angular/forms';
import { RsltnntddsService } from '../../../../../../services/rsltnntdds.service';
import { HelperService } from '../../../../../../services/helper.service';
import { RsltncrrdrsService } from '../../../../../../services/rsltncrrdrs.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import "rxjs/add/operator/debounceTime";




@Component({
  selector: 'app-registrar-intermediario',
  templateUrl: './registrar-intermediario.component.html',
  styleUrls: ['./registrar-intermediario.component.css'],
  providers: [RsltnpssService, RsltnntddsService, HelperService, RsltncrrdrsService]
})
export class RegistrarCorredorComponent implements OnInit {
  modulo: string = 'Registrar Corredores';
  Rsltnpss: Observable<any>;
  Rsltnntdds: Observable<any>;
  autocomplete: JQuery;
  razonsocial: string;
  of: string;
  rl: string = '/rsltncrrdrs';

  constructor(private _rgstr: RsltncrrdrsService, private router: Router, private _Rsltnpss: RsltnpssService,  private _Rsltnntdds: RsltnntddsService, private service: HelperService) {

  }

  ngOnInit() {
    this.Rsltnpss = this._Rsltnpss.getAll();
    this.Rsltnntdds = this._Rsltnntdds.getAll();
  }

  create(item) {
    this.service.postQuery(item, this.rl)
      .subscribe(
        item => {
          //   console.log(item[0].mensaje);
          alert(item.item.mensaje);
          this.router.navigate(['/admin/companiasreaseguradoras/corredor/list']);
        },
        error => console.log(<any>error));
  }

}
