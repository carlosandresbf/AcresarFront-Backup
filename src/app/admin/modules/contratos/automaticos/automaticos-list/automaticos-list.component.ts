import { Component, OnInit } from '@angular/core';
import { } from 'jquery';
import { } from 'morris.js';
import { } from 'jquery-knob';
import { } from 'bootstrap-datepicker';
import { } from 'jqueryui';
import { } from 'daterangepicker';
import { } from 'jquery.slimscroll';
import * as moment from 'moment';
import { Rsltncntrts } from '../../../../../models/rsltncntrts';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { HelperService } from '../../../../../services/helper.service';

@Component({
  selector: 'app-automaticos-list',
  templateUrl: './automaticos-list.component.html',
  styleUrls: ['./automaticos-list.component.css'],
  providers: [HelperService]
})
export class AutomaticosListComponent implements OnInit {

  calendar: JQuery;
  ls: Observable<any>;
  item: Rsltncntrts = new Rsltncntrts('', '', '', '', '', '', '', '', '');
  ready: boolean = false;
  items = { type: 'Automaticos'};
  modulo: string = 'Automaticos';
  constructor(private _ls: HelperService, private router: Router) {

    this.ls = this._ls.postQuery(this.items, '/rsltncntrts/filter').do(function () {
      setTimeout(function () {
        $("#myData").DataTable({ pagingType: 'full_numbers' }).draw();
        $("#myData").show();
        $(window).resize();
      }, 200);
    });
    }

  ngOnInit() {
  }

}
