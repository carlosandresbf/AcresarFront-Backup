import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsociacionLineasListComponent } from './asociacion-lineas-list.component';

describe('AsociacionLineasListComponent', () => {
  let component: AsociacionLineasListComponent;
  let fixture: ComponentFixture<AsociacionLineasListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsociacionLineasListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsociacionLineasListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
