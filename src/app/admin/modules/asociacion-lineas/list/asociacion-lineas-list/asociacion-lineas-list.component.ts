import { HelperService } from './../../../../../services/helper.service';
import { Observable } from "rxjs/Observable";
import { Component, OnInit, OnDestroy } from "@angular/core";
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

declare var $: any;
@Component({
  selector: "app-asociacion-lineas-list",
  templateUrl: "./asociacion-lineas-list.component.html",
  styleUrls: ["./asociacion-lineas-list.component.css"],
  providers:[HelperService]
})

export class AsociacionLineasListComponent implements OnInit {
  calendarfi: JQuery;
  calendarff: JQuery;
  modulo: string = 'Asociación de Contratos';
  formulario: FormGroup;
  ramos: any;
  tiposAsoc: any;
  dataReq:any; 
  lisRequest = false;
  showList = false;
  showAssoc = false;
  detail:any;
  selectedDetailItems = [];
  rl = '/asociaciondecontratos';
  list:any;

  constructor(
    private router: Router,
    public _http: HelperService
  ) {}

  ngOnInit() {
    this.loadDataTable();

    this._http.getQuery('/ramos').subscribe(
      res => {
        this.ramos = res;
      }
    );

    this._http.getQuery('/asociaciondecontratos/tipos').subscribe(
      res => {
        this.tiposAsoc = res;
      }
    );

    this.createForm();

    this.calendarfi = $("#fi").datepicker({
      format: "yyyy-mm-dd"
    });
    this.calendarff = $("#ff").datepicker({
      format: "yyyy-mm-dd"
    });

    $("#myDataTable").DataTable({});
    $("#myDataTable_filter").prepend(
      ' <label>A&ntilde;os de contrato: <select  class="color-fixed"><option >2018</option> <option >2017</option> <option >2016</option> <option >2015</option></select> &nbsp; &nbsp; </label>'
    );
  }

  reloadTable() {
    $("#myData").DataTable().destroy();
    this._http.getQuery(this.rl).subscribe(
      res => {
        this.list = res;
        const st = setTimeout(function () {
          $("#myData")
            .DataTable()
            .draw();
          $("#myData").fadeIn();
          $(window).resize();
          clearTimeout(st);
        }, 3000);

      }
    );
  }

  loadDataTable() {
    this._http.getQuery(this.rl).subscribe(
      res => {
        this.list = res;
        const st = setTimeout(function () {
          $("#myData")
            .DataTable()
            .draw();
          $("#myData").fadeIn();
          $(window).resize();
          clearTimeout(st);
        }, 1000);

      }
    );

  }

  createForm() {
    this.formulario = new FormGroup({
      tipoContrato: new FormControl('', Validators.required),
      idContrato: new FormControl('', Validators.required),
      idContratopk: new FormControl('', Validators.required),
      descripcion: new FormControl('', Validators.required),
      fInicio: new FormControl('', Validators.required),
      fFin: new FormControl('', Validators.required),
      ramo: new FormControl('', Validators.required),
      tipoAsociacion: new FormControl('', Validators.required)
    });
  }
  guardar() {

    const detail = $('.detailCheckbox:checked');
    this.selectedDetailItems= [];
    for (let i = 0; i <= detail.length - 1; i++) {
      let e = detail[i];
      this.selectedDetailItems.push(e.value);
    }

    let dataJson = this.formulario.value;
    dataJson['detalle'] = this.selectedDetailItems;
   
    this._http.postQuery(dataJson, this.rl).subscribe(
      item => {
        //   console.log(item[0].mensaje);
        alert(item.item.mensaje);
        this.formulario.reset();
        $("#cancelar").click();
        this.reloadTable();
        //this.router.navigate(["/admin/asociacionlineas"]);
      },
      error => console.log(<any>error)
    );
  }
  cargar(item){
    this.formulario.controls.idContratopk.setValue(item.a);
    this.formulario.controls.idContrato.setValue(item.o);
    this.formulario.controls.descripcion.setValue(item.c);
    this.formulario.controls.fInicio.setValue(item.r);
    this.formulario.controls.fFin.setValue(item.e);
    this.formulario.controls.tipoContrato.setValue(item.cat);
    this.lisRequest = false;
  }
  consultar() {
    this.lisRequest = true;
      if (this.formulario.controls.idContrato.value) {
      const item = { word: this.formulario.controls.idContrato.value};
      console.log(item);
      this._http.postQuery(item , '/contratos/search')
      .subscribe(
        res => {
          console.log(res);
          this.dataReq = res;
        }
      );
    }
  }
  ShowAssoc(){
this.showAssoc = false;
    if (this.formulario.controls.ramo.value !=''){
      this.showAssoc = true;
    }
  }

  getDetail(){
    if (this.formulario.controls.tipoAsociacion.value != 1 && this.formulario.controls.tipoAsociacion.value != 5){
      const item = { ramo: this.formulario.controls.ramo.value, tipo: this.formulario.controls.tipoAsociacion.value};
      this._http.postQuery(item, '/asociaciondecontratos/detail').subscribe(
        res => {
          this.showList = true;
          this.detail = res;  
           
        }
      );

    }else{
      this.showList = false;
      this.selectedDetailItems = [];
      this.detail = {};
    }
  }

}
