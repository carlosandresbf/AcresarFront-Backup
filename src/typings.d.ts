/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
  id: string;
}

interface JQuery {
  simpleMoneyFormat(options?: any): any;
}
interface JQuery {
  typeahead(options?: any): any;
}